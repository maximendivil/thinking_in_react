import React from 'react';
import ProductRow from './ProductRow';
import ProductCategoryRow from './ProductCategoryRow';

class ProductTable extends React.Component {
  render() {
    const filterText = this.props.filterText;
    const inStockOnly = this.props.inStockOnly;
    const products = this.props.products;
    let rows = [];
    let lastCategory = "";
    products.forEach(product => {
      if (product.name.indexOf(filterText) === -1) {
        return;
      }

      if (inStockOnly && !product.stocked) {
        return;
      }

      if (product.category !== lastCategory) {
        rows.push(<ProductCategoryRow category={product.category} />);
      }

      rows.push(<ProductRow product={product} />);
      lastCategory = product.category;
    });

    return (
      <div className="table-responsive">
        <table className="table table-bordered table-hover">
          <thead className="thead-dark">
            <tr>
              <th>
                Name
              </th>
              <th>
                Price
              </th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>      
    );
  }
}

export default ProductTable;
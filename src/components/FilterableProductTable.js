import React from 'react';
import SearchBar from './SearchBar';
import ProductTable from './ProductTable';

const PRODUCTS = [
  {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
  {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
  {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
  {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
  {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
  {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
];

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      inStockOnly: false
    };
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockOnlyChange = this.handleInStockOnlyChange.bind(this);
  }

  handleFilterTextChange(filterText) {
    this.setState({filterText: filterText});
  }

  handleInStockOnlyChange(inStockOnly) {
    this.setState({inStockOnly: inStockOnly});
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <SearchBar 
              filterText={this.state.filterText} 
              inStockOnly={this.state.inStockOnly}
              onFilterTextChange={this.handleFilterTextChange}
              onInStockOnlyChange={this.handleInStockOnlyChange} />
          </div>
        </div>
        
        <div className="row">
          <div className="col-md-12">
            <ProductTable 
              products={PRODUCTS} 
              filterText={this.state.filterText}
              inStockOnly={this.state.inStockOnly} />
          </div>
        </div>
      </div>
    )
  }
}

export default FilterableProductTable;
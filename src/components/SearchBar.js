import React from 'react';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockOnlyChange = this.handleInStockOnlyChange.bind(this);
  }

  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  handleInStockOnlyChange(e) {
    this.props.onInStockOnlyChange(e.target.checked);
  }

  render() {
    const filterText = this.props.filterText;
    const inStockOnly = this.props.inStockOnly;   

    return (
      <form className="mt-2">
        <div className="form-group">
          <input 
            type="text" 
            placeholder="Search..." 
            value={filterText} 
            onChange={this.handleFilterTextChange}
            className="form-control" />
        </div>        
        <div className="form-group form-check">
          <input 
            type="checkbox" 
            checked={inStockOnly} 
            onChange={this.handleInStockOnlyChange}
            className="form-check-input" /> 
          <label className="form-check-label">Only show products in stock</label>
        </div>
      </form>
    );
  }
}

export default SearchBar;